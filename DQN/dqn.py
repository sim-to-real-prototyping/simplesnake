# ehkä pitäisi tehdä A2C:llä: https://github.com/ShangtongZhang/DeepRL
# https://medium.com/@unnatsingh/deep-q-network-with-pytorch-d1ca6f40bfda
import math
import random
import numpy as np
#import matplotlib
#import matplotlib.pyplot as plt
from collections import namedtuple
from itertools import count
#from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T

from torch.distributions import Categorical

import torch
import torch.optim as optim
#from model import ActorCritic 
import numpy as np
from time import gmtime, strftime

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward', 'next_target_actual'))

BATCH_SIZE = 128
# n-step toteutuksessa gamma pitäisi kai olla aina 1.
GAMMA = 0.99
#model_name = "sin2_20200304155405_0.49"
model_name = "sin3_20200307180804_-1.05"
TESTIN_LEARNED_PARAMS = False
if TESTIN_LEARNED_PARAMS:
  load_from_model = True
else: 
  load_from_model = False

has_eps = not load_from_model
if has_eps:
  EPS_START = 0.9
  EPS_END = 0.05
  EPS_DECAY = 25000
else:
  EPS_START = 0.0
  EPS_END = 0.0
  EPS_DECAY = 1

TARGET_UPDATE = 30
lr = 0.00001
memory_size = 10000
alfa_R = 1/(memory_size*10)

#TODO:
# Toiiimo edes jos input on sin ja output on sama sin?
# Siirry yksinkertaisiin tapauksiin ja rupe improovaamaan niitä
# Testaa huomattavasti isommalla neural networkilla. Taitaa tulla raja vastaan

class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, state, action, next_state, reward, joint_target):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        
        self.memory[self.position] = Transition(state, action, next_state, reward, joint_target)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

class DQN(nn.Module):
  def __init__(self, state_size, joint_count, outputs = 3):
    super(DQN, self).__init__()
    # TODO: Pystyisikö batch norm käyttämään 1 kokoiselle batchille jos .eval()?
    self.state_size = state_size
    self.joint_count = joint_count
    self.action_space = outputs

    self.linear2 = nn.Linear(state_size, 100)
    self.linear4 = nn.Linear(100, 200)
    self.linear6 = nn.Linear(200, 50)
    self.linear7 = nn.Linear(50, self.action_space)
    self.linear8 = nn.Linear(200, 20)
    self.linear9 = nn.Linear(20, 1)


    def conv2d_size_out(size, kernel_size = 5, stride = 2):
      return (size - (kernel_size - 1) - 1) // stride  + 1

  def forward(self, state):
    #x = F.relu(self.bn1(self.conv1(x)))
    #out = state.view(,-1)
    batch_size = state.shape[0]
    out = state
    out = self.linear2(out)
    out = F.dropout(F.relu(out), p=0.01)
    out = F.relu(self.linear4(out))
    outI = F.dropout(F.relu(self.linear6(out)), p=0.01)
    outI = self.linear7(outI)
    outY = F.dropout(F.relu(self.linear8(out)), p=0.01)
    outY = self.linear9(outY)
    return outI, outY
      
#mielenkiintoinen: resize = T.Compose([T.ToPILImage(),T.Resize(40, interpolation=Image.CUBIC),T.ToTensor()])

def get_batch(memory):
  transitions = memory.sample(BATCH_SIZE)
  # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
  # detailed explanation). This converts batch-array of Transitions
  # to Transition of batch-arrays.
  return Transition(*zip(*transitions))

def get_eps(steps_done):
  eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
  return eps_threshold

def select_action(state, n_actions, device, policy_net, steps_done, joints_cnt):
  eps_threshold = get_eps(steps_done)
  sample = random.random()
  if sample > eps_threshold:
    with torch.no_grad():
      # t.max(1) will return largest column value of each row.
      # second column on max result is index of where max element was
      # found, so we pick action with the larger expected reward.
      action_values, next_act_val = policy_net(torch.tensor(state).float().to(device).unsqueeze(0))
      pol_acts = action_values.max(1)[1].view(1, -1)
      return pol_acts[0], next_act_val.item()

  else:
    #eps_acts = [[random.randrange(n_actions)]]
    eps_acts = [random.randrange(n_actions)]

    return torch.tensor(eps_acts, device=device, dtype=torch.long), random.random()*2-1

def compute_returns(rewards, G_avg):
  returns = []
  G = 0
  for step in reversed(range(len(rewards))):
    G = (rewards[step] - G_avg) + G

  return G

def get_state(env, joint, joint_index, prev_joint_pos_input, joint_target, is_first):
  if joint == env.state_info.elastic_joints[0]:
    joints = env.state_info.elastic_joints[:2]
  elif joint == env.state_info.elastic_joints[-1]:
    joints = env.state_info.elastic_joints[-2:]
  else: 
    joints = env.state_info.elastic_joints[joint_index-1:joint_index+2]
  
  state = env.to_state(joints)

  
  if joint == env.state_info.elastic_joints[0]:
    state = [0] + state + [1,0]
  elif joint == env.state_info.elastic_joints[-1]:
    state = state + [0] + [0,1]
  else:
    state = state + [0,0]
  
  #state = env.to_state([joint_index])
  state = state + [prev_joint_pos_input[joint][-1]-prev_joint_pos_input[joint][0]] + [joint_target, is_first]
  
  return state


def optimize_model(memory, device, policy_net, env, joint_count, target_net, optimizer, looppi, sum_loss):
  global TESTIN_LEARNED_PARAMS
  if len(memory) < BATCH_SIZE:
    return sum_loss, 0
  
  batch = get_batch(memory)
  next_states = torch.cat([s for s in batch.next_state if s is not None]).view(-1, env.state_size)

  state_batch = torch.cat(batch.state).view(-1, env.state_size)
  #joint_count pitäs kai poistaa
  action_batch = torch.cat(batch.action)#.view(-1, joint_count)

  reward_batch = torch.cat(batch.reward)
  next_target_actual = torch.cat(batch.next_target_actual)

  state_action_values, next_act_vals = policy_net(state_batch)


  state_action_values = state_action_values.gather(1, action_batch.unsqueeze(1)).squeeze()

  nsv, _ = target_net(next_states)
  next_state_values = nsv.max(1)[0].detach()
  
  expected_state_action_values = (next_state_values * GAMMA) + reward_batch#.unsqueeze(1).repeat(1, joint_count)

  target = expected_state_action_values.sum()
  output = state_action_values.squeeze().sum()

  batch_advantage = target - output
  batch_advantage = batch_advantage.cpu().detach().numpy()
  loss = F.smooth_l1_loss(output, target)
  #loss1 = (next_target_actual - next_act_vals.squeeze()).sum()
  loss2 = F.mse_loss(next_act_vals.squeeze(), next_target_actual)
  #loss = nn.MSELoss()
  #loss = loss(output, target)
  sum_loss += loss.cpu().detach().numpy()
  #loss = nn.MSELoss()(output.sum(), target.sum())


  # Optimize the model
  optimizer.zero_grad()
  (loss + loss2).backward()

  for param in policy_net.parameters():
    param.grad.data.clamp_(-1, 1)
  if not TESTIN_LEARNED_PARAMS:
    optimizer.step()

  return sum_loss, batch_advantage


def run_method(env_util, save_checkpoints = True, checkpoint_freq=50, version="perftest"):
  steps_done = 0
  env = env_util

  episode_durations = []
  joint_count = len(env.state_info.elastic_joints)


  # if gpu is to be used
  device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
  print(device)
  env.reset()

  # TRAINING:

  #init_screen = get_state(env, device)
  #init_screen = env.to_state().to(device)
  #_, _, screen_height, screen_width = init_screen.shape

  # Get number of actions from gym action space
  n_actions = env.action_space
  policy_net = DQN(env.state_size, joint_count, n_actions).to(device)
  target_net = DQN(env.state_size, joint_count, n_actions).to(device)
  target_net.load_state_dict(policy_net.state_dict())
  target_net.eval()

  #optimizer = optim.RMSprop(policy_net.parameters(), lr=lr, momentum=0.9)
  optimizer = optim.SGD(policy_net.parameters(), lr=lr, momentum=0.9)
  
  optimizer, policy_net = load_model(model_name, policy_net, optimizer, load_old_params=load_from_model)

  memory = ReplayMemory(memory_size)

  G_avg = 0
  R_avg = 0
  R_pure_avg = 0
  batch_advantage = 0

  action_val_hist = []
  batch_rew = 0

  # These two are pretty much the same thing.
  prev_joints = {}
  prev_joint_pos_input = {}

  # ---TRAINING---
  sum_loss = 0
  num_episodes = 500000
  joint_targets_pred = np.zeros(len(env.state_info.elastic_joints)).tolist()

  def is_snake_head(joint, env):
    return 1 if joint == env.state_info.elastic_joints[-1]  else 0

  for i_episode in range(num_episodes):

    # Initialize the environment and state
    env.reset()
    #last_screen = get_screen()
    #current_screen = get_screen()
    for t in count():

      # Select and perform an action

      #TODO: tämä pitää tehdä jokaiselle nivelelle erikseen:
      #* muuta pos forceksi
      #* simuloi kaikkien actioneiden jälkeen. Ota rewardi ja nivelten yhteen laskettu next_state rew vs vain oman nivelen rew

      joint_state = []
      joint_action = []

      # TODO: meneeköhän jotain pieleen koska R_pure_avg niin paljon jäljessä ihan vain saatumaa? Tai sitten oppii pelkästään taittamaan toiseen laitaan?
      # TODO: pitäisi olla 11/5 = 2.1 
      joint_targets_pred = joint_targets_pred[:-1] + [0] 

      joint_targets_temp = np.zeros(len(joint_targets_pred)).tolist()
      for i, joint in enumerate(env.state_info.elastic_joints):
        if joint not in prev_joint_pos_input:
          diff_history_deplay = 13
          prev_joint_pos_input[joint] = np.zeros(diff_history_deplay).tolist()
        
        prev_joint_pos_input[joint] = prev_joint_pos_input[joint][1:]
        prev_joint_pos_input[joint].append(env.to_state([joint])[0])

        state = get_state(env, joint, i, prev_joint_pos_input, joint_targets_pred[i], is_snake_head(joint, env))
        action, joint_target = select_action(state, n_actions, device, policy_net, steps_done, len(env.state_info.elastic_joints))
        joint_targets_temp[i] = joint_target
        joint_state.append(state)
        joint_action.append(action.cpu().detach().numpy())

      steps_done += 1
      
      #print(action.cpu().detach().numpy())
      #state, reward, positions, pure_reward = env.render(action.cpu().detach().numpy())
      joint_targets_pred = joint_targets_temp
      state, reward, positions, pure_reward, joint_targets_actual = env.render(joint_action, env.state_info.elastic_joints)
      # TODO: Hankkiudu eroon episode_dur hässäkästä
      episode_dur = 1000
      if t == episode_dur:
        done = True
      else:
        done = False
      #_, reward, done, _ = env.step(action.item())

      if not TESTIN_LEARNED_PARAMS:
        joint_targets_actual = joint_targets_actual[1:]

        if not done :
          for i, (joint, state, action, joint_target_actual) in enumerate(zip(env.state_info.elastic_joints,joint_state, joint_action, joint_targets_actual)):
            next_state = get_state(env, joint, i, prev_joint_pos_input, joint_target, is_snake_head(joint, env))

            G = compute_returns([reward], G_avg)

            #G_avg = (1-alfa_R)*G_avg + reward*alfa_R

            G = torch.tensor([G], device=device).float()

            joint_pos = env.to_state([joint])[0]

            if joint not in prev_joints:
              prev_joints[joint] = 1000
            prev_joint = prev_joints[joint]
            if abs(prev_joint - joint_pos ) > 0.01 or random.random() < 0.1:  
              prev_joints[joint] = joint_pos
              memory.push(torch.tensor(state).float().to(device), torch.tensor(action).to(device), torch.tensor(next_state).float().to(device), G, torch.tensor([joint_target_actual]).to(device))

          # Move to the next state
          #state = next_state

          #print(batch_advantage)
          G_avg = G_avg + batch_advantage*alfa_R
          R_avg = (1-alfa_R)*R_avg + alfa_R*reward
          alfa_R_pure = alfa_R * 10
          R_pure_avg = (1-alfa_R_pure)*R_pure_avg + alfa_R_pure*pure_reward
        
          # Perform one step of the optimization (on the target network)
          sum_loss, batch_advantage = optimize_model(memory, device, policy_net, env, joint_count, target_net, optimizer, t, sum_loss)

      if done:
        episode_durations.append(t + 1)
        rew_str = str(np.round(reward, 2))
        episode_loss_str = str(np.round(sum_loss/episode_dur, 2))
        G_avg_str = str(np.round(G_avg, 2))
        R_avg_str = str(np.round(R_avg, 2))
        R_pure_avg_str = str(np.round(R_pure_avg, 2))
        
        eps=get_eps(steps_done)
        eps_str = np.round(eps, 2)
        if len(memory) >= BATCH_SIZE:
          batch_rew = get_batch(memory).reward
          batch_rew = torch.cat(batch_rew).cpu().detach().numpy().mean()
          batch_rew_str = str(np.round(batch_rew,2))
        else:
          batch_rew_str = None
        action_np = action.cpu().detach().numpy()
        print("i", i_episode*episode_dur, "epis loss", episode_loss_str,  ", G_avg", G_avg_str, ", R_avg", R_avg_str,", R_pure_avg",R_pure_avg_str, ", latest rew", rew_str, ", eps", eps_str, ", latest act", action_np)
        sum_loss = 0
        #print("end of episode")
        #plot_durations()
        break
    # Update the target network, copying all weights and biases in DQN
    if i_episode % TARGET_UPDATE == 0:
      print("policy_net -> target_net")
      target_net.load_state_dict(policy_net.state_dict())
    if save_checkpoints and i_episode%checkpoint_freq== 0 and i_episode > 0:
      save_model(policy_net, optimizer, R_pure_avg, version)

  print('Complete')
  #env.render()
  #env.close()

def save_model(model, optimizer, R_avg, version):
  print(f"Saving checkpoint", version)

  R_avg_str = str(np.round(R_avg,2))
  
  time_str = strftime("%Y%m%d%H%M%S", gmtime())
  torch.save({
      'model': model.state_dict(),
      'optimizer': optimizer.state_dict()
      }, f"model_params/{version}_{time_str}_{R_avg_str}")




def load_model(file_name, model, optimizer, lr = None, load_old_params = False):
  def update_lr(lr, optimizer):
    for param_group in optimizer.param_groups:
      param_group['lr'] = lr

    return optimizer

  if load_old_params:
    # TODO: 
    # load latest
    # loadad smallest loss
    checkpoint = torch.load("model_params/"+file_name)
    model.load_state_dict(checkpoint['model'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    
    print("loaded params")
    if lr is not None:
      return update_lr(lr, optimizer)

  return optimizer, model