from tests import simple_test
from dqn import run_method

env_util = simple_test.EnvUtil()
run_method(env_util, version="dev", save_checkpoints = False, checkpoint__freq=49000)


"""
Iteration: 995000, returns -0.07, loss 0.14630999945290388, x position 0,y position 0, R_avg -0.054139021784067154, act tensor([1, 1, 1, 1], device='cuda:0'), rew 0.0
Iteration: 996000, returns -0.09, loss 0.2319000006020069, x position 0,y position 0, R_avg -0.05550092086195946, act tensor([1, 1, 1, 1], device='cuda:0'), rew 0.0
Iteration: 997000, returns -0.1, loss 0.15467000005580483, x position 0,y position 0, R_avg -0.054828058928251266, act tensor([1, 1, 1, 1], device='cuda:0'), rew 0.0
Iteration: 998000, returns -0.09, loss 0.18420000072941184, x position 0,y position 0, R_avg -0.05483126640319824, act tensor([1, 1, 1, 1], device='cuda:0'), rew 0.0
Iteration: 999000, returns -0.11, loss 0.1680000009443611, x position 0,y position 0, R_avg -0.05460353195667267, act tensor([1, 1, 1, 1], device='cuda:0'), rew 0.0
"""
