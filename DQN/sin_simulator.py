import torch
from env_utils import EnvParams
import pybullet as p
from snake_util import steer
import time
import math
import numpy as np

from dqn import run_method
from env_utils import EnvParams
import argparse

class SinEnv(EnvParams):
  def __init__(self):
    super(SinEnv, self).__init__()
    self.ts = 0

  
  def _target_position(self, joint, ts):
    tsf = 0.1
    return math.sin((ts*tsf+joint*math.pi/8))*self.state_info.movent_quantity


  def get_correct_act(self, sin_action):
    if sin_action > 0.7:
      return 0
    elif sin_action > 0.1:
      return 1
    elif sin_action > -0.1:
      return 2
    elif sin_action > -0.7:
      return 3
    else:
      return 4


  def _reward(self, model_actions, sin_actions):

    current_positions = self.to_state()
    rew = 0
    for model_action, sin_action, current_pos in zip(model_actions, sin_actions, current_positions):

      correct_action = self.get_correct_act(sin_action)

      val = -(abs(correct_action - model_action[0])/3)**2
      rew += val

    return rew

#käärmeen olisi hyvä olla ainakin niin pitkä, että kaksi mutkaa. Eli vähintään yhden aallonpituuden.
#optimaalisesti vain käärmeen mutkien oikean sivun pitäisi osua maahan.
    
  def render(self, model_actions, joints):
    #joints = self.state_info.elastic_joints
    actions = []
    #for joint, action in zip(np.flip(np.array(joints)), np.flip(model_actions)):
    for joint, action in zip(np.array(joints), model_actions):
      
      sin_action = self._target_position(joint, self.ts)

      actions.append(sin_action)

      m_steering, force = self.state_info.action_to_position_and_force(self.get_correct_act(sin_action), joint, self.action_space)

      self.state_info.set_joint_status(joint, m_steering, force)
      steer(joint, m_steering, self.sphereUid, force)

    phantom_next_joint_sin = self._target_position(1- 3, self.ts)
    actions.append(phantom_next_joint_sin)

    self.ts += 1

    reward = self._reward(model_actions, actions)

    for forward in range(self.timesteps_per_render):
      self._enforce_constraints()
      p.stepSimulation()
    

    state = self.to_state()
    #positions = self._current_joint_positions()
    positions = None

    return state, reward, positions, reward, actions

env_util = SinEnv()

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-t','--terminal', type=str)

args = parser.parse_args()


#env_util = EnvParams()
run_method(env_util, version="sin"+args.terminal, save_checkpoints = True)
