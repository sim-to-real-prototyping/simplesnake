import pybullet as p
import time
import math
from state_info import StateInfo

def steer(joint, targetPos, sphereUid, force = 20):
  p.setJointMotorControl2(sphereUid,
                        joint,
                        p.POSITION_CONTROL,
                        targetPosition=targetPos,
                        force=force)

def create_snake():
  sphereRadius = 0.5
  sphereRadiusa = 0.1
  sphereRadius1 = 0.25
  sphereRadius2 = 0.25
  #colBoxId = p.createCollisionShapeArray([p.GEOM_BOX, p.GEOM_SPHERE],radii=[sphereRadius+0.03,sphereRadius+0.03], halfExtents=[[sphereRadius,sphereRadius,sphereRadius],[sphereRadius,sphereRadius,sphereRadius]])
  colBoxId = p.createCollisionShape(p.GEOM_BOX,halfExtents=[sphereRadius1, sphereRadius, sphereRadius2])
  colBoxId2 = p.createCollisionShape(p.GEOM_BOX,halfExtents=[sphereRadius1, sphereRadiusa, sphereRadius2])

  mass = 1
  visualShapeId = -1
  state_info = StateInfo()

  link_Masses = []
  linkCollisionShapeIndices = []
  linkVisualShapeIndices = []
  linkPositions = []
  linkOrientations = []
  linkInertialFramePositions = []
  linkInertialFrameOrientations = []
  indices = []
  jointTypes = []
  axis = []

  for block in range(10):
    # Ensimmäinen solid osa on itsenäinen multibodyssä
    # ensimmäinen nivel osa
    i=block*3 + 0
    link_Masses.append(1)
    linkCollisionShapeIndices.append(colBoxId2)
    linkVisualShapeIndices.append(-1)
    dist = (sphereRadiusa/2 + sphereRadius / 2)*2
    linkPositions.append([0, dist + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_FIXED)
    axis.append([0, 0, 1])
    i=block*3 + 1
    # Toinen nivelen osa
    link_Masses.append(1)
    linkCollisionShapeIndices.append(colBoxId2)
    linkVisualShapeIndices.append(-1)
    linkPositions.append([0, sphereRadiusa * 2.0 + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_REVOLUTE)
    axis.append([0, 0, 1])
    state_info.add_elastic_joint(i)
    # toinen solid osa
    i=block*3 + 2
    link_Masses.append(1)
    linkCollisionShapeIndices.append(colBoxId)
    linkVisualShapeIndices.append(-1)
    dist = (sphereRadiusa/2 + sphereRadius / 2)*2
    linkPositions.append([0, dist + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_FIXED)
    axis.append([0, 0, 1])

  basePosition = [0, 0, 0]
  baseOrientation = [0, 0, 1, 0]
  sphereUid = p.createMultiBody(mass,
                                colBoxId,
                                visualShapeId,
                                basePosition,
                                baseOrientation,
                                linkMasses=link_Masses,
                                linkCollisionShapeIndices=linkCollisionShapeIndices,
                                linkVisualShapeIndices=linkVisualShapeIndices,
                                linkPositions=linkPositions,
                                linkOrientations=linkOrientations,
                                linkInertialFramePositions=linkInertialFramePositions,
                                linkInertialFrameOrientations=linkInertialFrameOrientations,
                                linkParentIndices=indices,
                                linkJointTypes=jointTypes,
                                linkJointAxis=axis)

  p.setGravity(0, 0, -9.81)
  p.setRealTimeSimulation(0)

  anistropicFriction = [0.05, 0.1, 0.1]
  p.changeDynamics(sphereUid, -1, lateralFriction=2, anisotropicFriction=anistropicFriction)
  for i in range(p.getNumJoints(sphereUid)):
    p.getJointInfo(sphereUid, i)
    p.changeDynamics(sphereUid, i, lateralFriction=2, anisotropicFriction=anistropicFriction)

  p.enableJointForceTorqueSensor(sphereUid, 0)

  for joint in state_info.elastic_joints:
    p.enableJointForceTorqueSensor(sphereUid, joint, True)

  return sphereUid, state_info