# https://github.com/nikhilbarhate99/Actor-Critic-PyTorch/blob/master/model.py 
import torch
import torch.optim as optim
from model import ActorCritic 
import numpy as np
from time import gmtime, strftime

def run_method(env_util, save_checkpoints = True, checkpoint__freq=200, version="perftest"):

  def save_model(epoch, old_epoch, avg_loss, actor_critic, optimizerAC, R_avg, R_avg_checkpoint):
    tot_epoch = old_epoch + epoch
    print(f"Saving epoch. Epoch loss {avg_loss}. epoch {tot_epoch}")
    
    time_str = strftime("%Y%m%d%H%M%S", gmtime())
    torch.save({
        'epoch': tot_epoch,
        'model_actor_critic_dict': actor_critic.state_dict(),
        #'model_actor_dict': actor.state_dict(),
        #'model_critic_dict': critic.state_dict(),
        'optimizer_actor_critic_dict': optimizerAC.state_dict(),
        #'optimizer_actor_dict': optimizerA.state_dict(),
        #'optimizer_critic_dict': optimizerC.state_dict(),
        "R_avg": R_avg
        }, f"model_params/{version}_{time_str}_{R_avg_checkpoint}_{avg_loss}")

  device = env_util.device

  avg_loss = 0
  R_avg = 0
  R_avg_checkpoint = 0
  alfa_R = 0.0001

  # simple test case lr = 0.001 on täysin ylivoimainen 0.00001 verrattuna.
  # reward -1 - -2, ku actort lr 0.0001 ja cr 0.000005

  lr_ac = 0.00001
  actor_critic = ActorCritic(device, env_util.state_size, action_space=env_util.action_space, history_size = env_util.min_history, joint_count = len(env_util.state_info.elastic_joints)).to(device)
  optimizerAC = optim.SGD(actor_critic.parameters(), lr=lr_ac, momentum=0.95) # , momentum=0.99

  old_epoch = 0

  load_old_params = False
  if load_old_params:
    # TODO: 
    # load latest
    # loadad smallest loss
    checkpoint = torch.load("model_params/dev2_20200219100139_0.6_0.9")
    actor_critic.load_state_dict(checkpoint['model_actor_critic_dict'])
    optimizerAC.load_state_dict(checkpoint['optimizer_actor_critic_dict'])
    old_epoch = checkpoint['epoch']
    R_avg = checkpoint['R_avg']
    
    print("loaded params")

  def update_lr(lr, optimizer):
    for param_group in optimizer.param_groups:
      param_group['lr'] = lr

    return optimizer

  optimizerAC = update_lr(lr_ac, optimizerAC)
  actor_critic.train()

  """
  * seed voisi olla selkeämpi tapa tehdä batchiä kuin nykyinen.
  * googlaa tutkimuspaperi - koodi repoja ja etsi snake ratkaisuja
  ** https://paperswithcode.com
  ** deepmindilla täytyy olla vanhoja papereita joissa yksinkertaisia ratkaisuja matojen koulutukseen.
  """

  dt = 1. / 6.
  n_iters = 3000000
  batch_size = 300
  print_monitorin_interval = 1200 // batch_size

  state_history = np.zeros((env_util.min_history, env_util.state_size))
  values = []
  next_pos_loss = []
  rewards = []
  masks = []
  log_probs = []
  actor_critic_losses = []
  since_last_update = 0

  env_util.reset()


  # TODO: älä ota kaikkia steppejä yhtä aikaa, vaan eka nivel tekee ekan simulaatio liikeen, toka sen jälkeen jne
  # * voisin jopa tehdä outputin joka ennustaa action delayden määrää
  # TODO: laita default komennot siniaalloiksi ja malli vain ennustaa korjaukset sin aaltoon.

  # TODO: CNN takaisin? alustettuna erilaisilla sin käyrillä?

  # TODO: Voisiko General Value Functionia käyttää tähän? Chapter 17.1
  # TODO: tee eligibitiity trace -> tee aluksi eteenpäin katsova optimointi koska sellainen on helpompaa
  # TODO: voisi ennustaa yhden actionin. eli yksi nivel action per timestep. action space = 4*3 (tai 4*2). Eli ei mikään mahdoton.
  # TODO: Voisiko LSTM toimia?
  # TODO: Entä jos tämä ongelma ei olekkaan MDP problem?
  # TODO: lisää entropy: https://github.com/tensorflow/agents/blob/master/tf_agents/agents/sac/sac_agent.py. Vai onko R_avg ~ entropy. ei kai
  # TODO: target_input = (next_time_steps.observation, next_actions). V -> Q


  def compute_returns(next_value, rewards, R_avg, gamma=0.99):
    returns = []
    G = 0
    for step in reversed(range(len(rewards))):
      G = (rewards[step] - R_avg) + G
      G = rewards[step] + G

    return next_value.detach() + G

  batches_done = 0
  for ij in range(n_iters):

    if env_util.early_jump_ended():
      dist, value, next_pos_pred = actor_critic(torch.tensor(state_history[:1]).to(device).float(), ij)
      #dist, value = actor(torch.tensor(state_history[:1]).to(device).float()), critic(torch.tensor(state_history[:1]).float().to(device))
      action = dist.sample()
      next_state, reward, joint_positions = env_util.render(action.cpu().numpy()[0])
    
      state_history = np.roll(state_history,1, axis=0)
      state_history[0] = next_state.cpu().detach().numpy()
      
      if reward is None:
        continue

      # Mitä pienempi prob, sitä isompi -log_prob. Eli tämä on ln osuus?
      log_prob = dist.log_prob(action).sum()

      log_probs.append(log_prob)
      rewards.append(reward)
      values.append(value)
      next_pos_l = (next_pos_pred-torch.tensor(joint_positions).to(device)).pow(2).sum()
      next_pos_loss.append(next_pos_l)

      state = next_state
      if len(rewards) >= env_util.min_history:
        #next_value = critic(torch.tensor(state_history[:1]).float().to(device))
        _, next_value, _ = actor_critic(torch.tensor(state_history[:1]).float().to(device), 13)
        returns = compute_returns(next_value, rewards, R_avg)
        advantage = returns - values[0]
        
        # varmista että n-step toimii
        R_avg = R_avg + advantage.cpu().detach().mean()*alfa_R

        actor_loss = -(log_probs[0] * advantage.item())
        critic_loss = advantage.pow(2)

        tot_loss = actor_loss + critic_loss  #+ next_pos_loss[0]
        actor_critic_losses.append(tot_loss)

        def calibrate_loss(loss, losstype):
          # To prevent cuda exception
          if loss.cpu().detach().item() > 1000000:
            print(f"huge loss: {loss.cpu().detach().item()}, {losstype}")

          return loss

        #print(f"a loss: {actor_loss.cpu().detach().item()}, c loss {critic_loss.cpu().detach().item()}, {state_history}, {log_prob}, \
        # {advantage.cpu().detach()}")
        calibrate_loss(actor_loss, "actor")
        calibrate_loss(critic_loss, "critic")

        rewards = rewards[1:]
        masks = masks[1:]
        values = values[1:]
        log_probs = log_probs[1:]
        next_pos_loss = next_pos_loss[1:]


        since_last_update += 1
        if batch_size <= since_last_update:
          actor_critic_losses_stacked = torch.stack((actor_critic_losses)).mean()
          current_loss = np.round(actor_critic_losses_stacked.cpu().detach().numpy(), 2)

          avg_loss += current_loss
          R_avg_checkpoint += current_loss
          if batches_done % checkpoint__freq == (checkpoint__freq - 1) and save_checkpoints:
            avg_loss_str = np.round(avg_loss, 1)
            R_avg_checkpoint_str = np.round(R_avg_checkpoint / checkpoint__freq, 1)
            save_model(ij, old_epoch, avg_loss_str, actor_critic, optimizerAC, R_avg, R_avg_checkpoint_str)
            R_avg_checkpoint = 0
          if batches_done % print_monitorin_interval == 0:
            ret = str(np.round(returns.sum().cpu().detach().numpy(), 2))
            x_coord, y_coord = env_util.get_xy_coord()
            loss_round = np.round(avg_loss/print_monitorin_interval, 2)
            R_avg_round = np.round(R_avg.cpu().item(), 2)
            reward_round = np.round(reward, 2)
            next_value_round = np.round(next_value.cpu().detach().item(), 2)
            x_coord_round = np.round(x_coord, 2)
            y_coord_round = np.round(y_coord, 2) 
            acts = action.cpu().detach().numpy()
            print(f'Iter: {ij}, returns: {ret}, loss: {loss_round}, R_avg: {R_avg_round}, rew: {reward_round}, next_value: {next_value_round}, x: {x_coord_round},y: {y_coord_round}, act: {acts}')
            avg_loss = 0
          
          optimizerAC.zero_grad()
          actor_critic_losses_stacked.backward()
          batches_done += 1

          optimizerAC.step()

          since_last_update = 0

          log_probs = []
          values = []
          rewards = []
          returns = []
          masks = []
          next_pos_loss = []
          actor_critic_losses = []


        #import time
        #time.sleep(dt)

# 