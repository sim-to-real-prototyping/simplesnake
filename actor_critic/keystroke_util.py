import pybullet as p
from snake_util import steer

def keyboard_actions(state_info, sphereUid):
  keys = p.getKeyboardEvents()
  force = 20
  for k, v in keys.items():
    if (k == p.B3G_RIGHT_ARROW and (v & p.KEY_WAS_TRIGGERED)):
      m_steering = -state_info.movent_quantity
      state_info.set_joint_status(1, m_steering)
      state_info.set_joint_status(1, m_steering, force)
      steer(1, m_steering, sphereUid)
    if (k == p.B3G_LEFT_ARROW and (v & p.KEY_WAS_TRIGGERED)):
      m_steering = state_info.movent_quantity
      state_info.set_joint_status(1, m_steering)
      state_info.set_joint_status(1, m_steering, force)
      steer(1, m_steering, sphereUid)
    # 122 & 120 = z ja x
    if (k == 122 and (v & p.KEY_WAS_TRIGGERED)):
      m_steering = state_info.movent_quantity
      state_info.set_joint_status(4, m_steering)
      steer(4, m_steering, sphereUid)
    if (k == 120 and (v & p.KEY_WAS_TRIGGERED)):
      m_steering = -state_info.movent_quantity
      state_info.set_joint_status(4, m_steering)
      steer(4, m_steering, sphereUid)