import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Categorical, Normal


def compute_returns(next_value, rewards, masks, gamma=0.99):
    R = next_value
    returns = []
    for step in reversed(range(len(rewards))):
        R = rewards[step] + gamma * R * masks[step]
        returns.insert(0, R)
    return returns

class ActorCritic(nn.Module):
  def __init__(self, device, state_size=10, action_space=3, channels = 1, joint_count = 4, history_size = 10):
    super(ActorCritic, self).__init__()
    self.state_size = state_size
    self.action_space = action_space
    self.linear2 = nn.Linear(state_size, 300)
    self.linear4 = nn.Linear(300, 400)
    self.bn2 = nn.BatchNorm1d(num_features=200)

    self.linear5 = nn.Linear(400, 100)
    self.linear_actor_mean = nn.Linear(100, joint_count)
    self.linear_actor_std = nn.Linear(100, joint_count)
    self.linear_critic = nn.Linear(100, 1)
    self.next_positions = nn.Linear(100, joint_count)
    self.device = device

  def forward(self, state, loopp):
    out = state.view(1,-1)
    out = self.linear2(out)
    out = F.dropout(F.relu(out), p=0.2)
    out = F.dropout(F.relu(self.linear4(out)), p=0.2)
    out = F.dropout(F.relu(self.linear5(out)), p=0.2)

    # actor
    out_am = self.linear_actor_mean(out)
    out_as = self.linear_actor_std(out)
    #outa = outa.view(-1,self.action_space)
    #outa = F.softmax(outa, dim=-1)    
    #distribution = Categorical(outa)

    # TODO: replace sigmoid with tanh
    out_am = torch.clamp(out_am, min=-1.0, max=1.0)
    out_as = torch.clamp(out_as, min=0.0001, max=0.5)
    #out_am = F.sigmoid(out_am)
    #out_as = F.sigmoid(out_as)
    proba = 0.3
    #probas = [proba for a in range(out_am.shape[0])]
    if loopp % 4000 == 0:
      print(out_am.cpu().detach().numpy(), "m") 
      print(out_as.cpu().detach().numpy(), "s")
    distribution = Normal(out_am, out_as)
    
    # critic
    outc = self.linear_critic(out)
    
    # prediction for next positions
    next_pos = self.next_positions(out)


    return distribution, outc, next_pos

"""
class Critic123(nn.Module):
  def __init__(self, state_size=10, action_space=3, channels = 1,  joint_count = 4, history_size = 10):
    super(Critic, self).__init__()
    self.state_size = state_size
    self.action_space = action_space
    print(state_size, "state_size c")
    self.linear2 = nn.Linear(state_size, 100)
    self.linear4 = nn.Linear(100, 20)
    self.linear5 = nn.Linear(20, 1)

  def forward(self, state):
    out = state.view(1,-1)

    out = F.dropout(F.relu(self.linear2(out)), p=0.4)
    out = F.dropout(F.relu(self.linear4(out)), p=0.4)
    out = self.linear5(out)
    return out
"""