from actor_critic import run_method
from env_utils import EnvParams
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-t','--terminal', type=str)

args = parser.parse_args()


env_util = EnvParams()
run_method(env_util, version="dev" + args.terminal, save_checkpoints = True)
