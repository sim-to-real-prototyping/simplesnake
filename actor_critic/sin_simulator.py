from env_utils import EnvParams
import pybullet as p
from snake_util import steer
import time
import math
import numpy as np


from actor_critic import run_method
from env_utils import EnvParams
import argparse

class SinEnv(EnvParams):
  def __init__(self):
    super(SinEnv, self).__init__()
    self.ts = 0

  
  def _target_position(self, joint, ts):
    tsf = 0.02
    return math.sin((ts*tsf+joint*math.pi/6))*self.state_info.movent_quantity


  def _reward(self, model_actions, sin_actions):
    act_diff = np.absolute(np.array(model_actions) - np.array(sin_actions))
    reward = -np.sum(act_diff)
    return reward


  def render(self, model_actions):
    joints = self.state_info.elastic_joints
    actions = []
    for joint, action in zip(joints, model_actions):
      sin_action = self._target_position(joint, self.ts)
      print(sin_action)

      actions.append(sin_action)
      m_steering, force = self.state_info.action_to_position_and_force(sin_action, self.state_info)

      self.state_info.set_joint_status(joint, m_steering, force)
      steer(joint, m_steering, self.sphereUid, force)
    
    self.ts += 1

    for forward in range(self.timesteps_per_render):
      self._enforce_constraints()
      p.stepSimulation()
    
    reward = self._reward(model_actions, actions)

    state = self._to_state().to(self.device)
    positions = self._current_joint_positions()


    return state, reward, positions

env_util = SinEnv()

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-t','--terminal', type=str)

args = parser.parse_args()


#env_util = EnvParams()
run_method(env_util, version="sin", save_checkpoints = True)


if False:
  #env_util = EnvParams()
  dt = 0.0001
  ts = 0
  while True:
    targetPos = 1

    for joint in env_util.state_info.elastic_joints:
      f = force(joint, ts)
      targetPos = 0.8
      if f < 0:
        direction = -1
      else:
        direction = 1
      if joint == 1:
        print(f)
        
      #steer(joint, direction*targetPos, env_util.sphereUid, force = abs(f)*10)
      steer(joint, f, env_util.sphereUid, force = 20)
      p.stepSimulation()

    #time.sleep(dt)
    ts += 1
  # state, reward, positions

