import torch
import numpy as np

class TestStateInfo:
  def __init__(self):
    self.elastic_joints = [1,4,7,10]

class EnvUtil:
  def __init__(self):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    self.device = device
    print("device", device)
    self.action_space = None
    self.state_info = TestStateInfo()
    self.last_out = None
    self.zeros = [0,0,0,0,0,0,0,0,0,0]
    self.ones = [1,1,1,1,1,1,1,1,1,1]
    # EnvUtil must define these
    self.state_size = 10
    self.min_history = 5

  def early_jump_ended(self):
    return True
  
  def reset(self):
    self.last_out = 0
    return  torch.tensor(self.zeros).to(self.device).float()

  """
  * jos edellinen output oli [1,1,1,1,1,1,1,1,1,1], niin reward on (actions - 1).mean()
  * jos edellinen output oli [0,0,0,0,0,0,0,0,0,0], niin reward on (1- actions).mean()
  """
  def render(self, actions): # actions: torch.Size([4]). acion on 0,1 tai 2. 
    if self.last_out == 0:
      reward = (-np.abs(np.abs(actions.cpu().numpy()-1))).mean()
      state = torch.tensor(self.ones).to(self.device).float()
      self.last_out = 1
    else:
      reward = (-np.abs(actions.cpu().numpy())).mean()
      state = torch.tensor(self.zeros).to(self.device).float()
      self.last_out = 0

    # state example:
    # tensor([1.0577, 0.5012, 0.0678, 0.5538, 0.1563, 0.1194, 0.7284, 2.5614, 0.8815,0.9203], device='cuda:0')
    return state, reward, state[:len(self.state_info.elastic_joints)]

  def get_xy_coord(self):
    return 0, 0

def get_env_util():
  env_util = EnvUtil()
  return env_util


"""
a=EnvUtil()
a.reset()
#print(a.render(torch.tensor([1,1,1,1])))
state, reward = a.render(torch.tensor([0,0,0,0]))
assert reward == -1
"""

