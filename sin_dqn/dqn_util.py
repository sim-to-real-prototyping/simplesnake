
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
import torch
import torch.optim as optim
#from model import ActorCritic 
import numpy as np
from collections import namedtuple
import random

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))

class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, state, action, next_state, reward):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        
        self.memory[self.position] = Transition(state, action, next_state, reward)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class DQN(nn.Module):
  def __init__(self, state_size, outputs = 3):
    super(DQN, self).__init__()
    # TODO: Pystyisikö batch norm käyttämään 1 kokoiselle batchille jos .eval()?
    self.state_size = state_size
    self.action_space = outputs

    self.linear2 = nn.Linear(state_size, 100)
    self.linear4 = nn.Linear(100, 200)
    self.linear6 = nn.Linear(200, 50)
    self.linear7 = nn.Linear(50, self.action_space)


    def conv2d_size_out(size, kernel_size = 5, stride = 2):
      return (size - (kernel_size - 1) - 1) // stride  + 1

  def forward(self, state):
    #x = F.relu(self.bn1(self.conv1(x)))
    #out = state.view(,-1)
    batch_size = state.shape[0]
    out = state
    out = self.linear2(out)
    out = F.dropout(F.relu(out), p=0.01)
    out = F.relu(self.linear4(out))
    out = F.dropout(F.relu(self.linear6(out)), p=0.01)
    out = self.linear7(out)
    return out



def save_model(model, optimizer, R_avg, version):
  print(f"Saving checkpoint", version)

  R_avg_str = str(np.round(R_avg,2))
  
  time_str = strftime("%Y%m%d%H%M%S", gmtime())
  torch.save({
      'model': model.state_dict(),
      'optimizer': optimizer.state_dict()
      }, f"model_params/{version}_{time_str}_{R_avg_str}")


def load_model(file_name, model, optimizer, lr = None, load_old_params = False):
  def update_lr(lr, optimizer):
    for param_group in optimizer.param_groups:
      param_group['lr'] = lr

    return optimizer

  if load_old_params:
    checkpoint = torch.load("model_params/"+file_name)
    model.load_state_dict(checkpoint['model'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    
    print("loaded params")
    if lr is not None:
      return update_lr(lr, optimizer)

  return optimizer, model