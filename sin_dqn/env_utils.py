import numpy as np
import queue
from snake_util import create_snake, steer, create_track
import torch
from state_info import StateInfo
import pybullet as p
from shapely.geometry import Point
from shapely.geometry import LineString
import random
import math


class EarlyJumpEnded:
  def __init__(self, sphereUid):
    self.early_rise_happenned = False
    self.object_landed = False
    self.sphereUid = sphereUid

  def update(self):
    cubePos, cubeOrn = p.getBasePositionAndOrientation(self.sphereUid)
    y,x,z=cubePos
    if self.object_landed:
      return True
    elif z > 0.26:
      self.early_rise_happenned = True
      return False
    elif self.early_rise_happenned and z < 0.251:
      self.object_landed = True
      return True
    else:
      return False

class EnvParams:
  def __init__(self):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("device", device)
    self.number_of_inp_vars = 1
    p.connect(p.GUI)
    plane = p.createCollisionShape(p.GEOM_PLANE)
    p.createMultiBody(0, plane)
    
    sphereUid, state_info = create_snake()
    create_track()

    joint_count = len(state_info.elastic_joints.horizontal)
    self.action_space = 5
    self.sphereUid = sphereUid
    self.state_info = state_info
    self.device = device
    #self.state_size = len(self.state_info.elastic_joints)*self.number_of_inp_vars
    self.state_size = 21 # 10 joints and latest reward
    self.rewards_per_return = 1
    self.reward_history = 25
    self.early_jump = EarlyJumpEnded(sphereUid)
    self.timesteps_per_render = 30
    self.history_coords = []
    self.history_positions = []
    self.history_pos_max = 20

  # TODO: Likely this can be removed.
  def early_jump_ended(self):
    jump_ended = self.early_jump.update()
    if jump_ended is False:
      p.stepSimulation()
    return jump_ended

  def reset(self):
    return

  def _get_head_tail(self, track):
    max_dist = 0
    max_dist_state = None
    assert len(track) > 0
    for state in track:
      # track consists of ((x1,y1),(x2,y2))
      p1 = state[0]
      p2 = state[1]
      distance = math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )

      if distance > max_dist:
        max_dist = distance
        max_dist_state = state

    return max_dist_state

  def _calculate_joint_ang_diff(self):
    max_ind = 10
    if len(self.history_positions) < max_ind+1:
      return 0

    # TODO: flip can be removed if indexing is fixed
    diff1 = np.flip(np.array(self.history_positions[-max_ind]) - np.array(self.history_positions[-1]))
    too_small_change_s = np.sum(np.abs(diff1) < 0.4) / 6
    #too_small_change_l = np.sum(np.abs(diff1) < 0.8) / 3
    return -(too_small_change_s)# + too_small_change_l)


  def _calculate_corr(self):
    max_ind = 6
    positions = self._current_joint_positions(self.state_info.elastic_joints.horizontal)

    self.history_positions.append(positions)

    if len(self.history_positions) < max_ind + 1:
      return 0
    if len(self.history_positions) > self.history_pos_max:
      self.history_positions = self.history_positions[1:]

    # TODO: flip can be removed if indexing is fixed
    diff1 = np.flip(np.array(self.history_positions[-1]) - np.array(self.history_positions[-max_ind//2]))
    diff2 = np.flip(np.array(self.history_positions[-max_ind//2]) - np.array(self.history_positions[-max_ind]))

    corr = diff2[:-1] - diff1[1:]
    closely_correlated_count = np.sum(np.logical_and(np.sign(diff1[:-1]) == np.sign(diff2[1:]), abs(corr) < 0.2))
    corr_score = -closely_correlated_count/3
    #corr_score = np.sum(np.sign(diff1[:-1]) == np.sign(diff2[1:]))/2
    #corr_score = -corr_score/5

    # Why?
    #print(np.sign(diff1[:-1]) == np.sign(diff2[1:]), len(self.history_positions))
    #print()
    return corr_score

  def _rnd_penalty(self, distance):
    return -min(0.1, random.random() / (abs(distance)))
    #return 0

  def _collapsing_penalty(self):
    y_t, x_t, z_t = p.getLinkState(self.sphereUid, self.state_info.elastic_joints.horizontal[-1], 1)[0] 
    y_h, x_h, z_h = p.getLinkState(self.sphereUid, self.state_info.elastic_joints.horizontal[0], 1)[0] 

    dist = math.hypot(x_h - x_t, y_h - y_t)
    penalty = min(0, dist - 8.5)
    return -penalty**2

  def _reward(self):
    head_new, tail_new = self._head_and_tail_coords()
    self.history_coords.append((head_new, tail_new))

    if len(self.history_coords) < self.reward_history+1:
      return 0

    head_old, tail_old = self._get_head_tail(self.history_coords[0:5])
    self.history_coords = self.history_coords[1:]
    distance = self._calculate_distance(head_old, tail_old, head_new, tail_new)

    corr_score = self._calculate_corr()

    too_small_change_penalty = self._calculate_joint_ang_diff()
    rnd_penalty = self._rnd_penalty(distance)

    collapsing_penalty = self._collapsing_penalty()
    return distance #+ corr_score + too_small_change_penalty + rnd_penalty + collapsing_penalty


  def _enforce_constraints(self):
    def constrain(joints, movent_quantity):
      positions = self._current_joint_positions(joints)
      velocities = [p.getJointState(self.sphereUid,pos)[1] for pos in joints]
      self._update_joint_constraint_force(positions, velocities, joints, movent_quantity)

    constrain(self.state_info.elastic_joints.horizontal, self.state_info.movent_quantity_hor)
    constrain(self.state_info.elastic_joints.vertical, self.state_info.movent_quantity_ver)

  def render(self, actions, joints):
    # joints = self.state_info.elastic_joints
    for joint, action in zip(joints, actions):
      m_steering, force = self.state_info.action_to_position_and_force(action, joint, self.action_space)
      self.state_info.set_joint_status(joint, m_steering, force)
      steer(joint, m_steering, self.sphereUid, force)

    for forward in range(self.timesteps_per_render):
      self._enforce_constraints()
      p.stepSimulation()
      #import time
      #time.sleep(1)
    reward = self._reward()
    print(reward)

    state = self.to_state()#.to(self.device)
    positions = self._current_joint_positions(joints)

    return state, reward, positions, None, None

  def _update_joint_constraint_force(self, positions, velocities, joints, movent_quantity):
    state_info = self.state_info
    
    for pos, vel, joint in zip(positions, velocities, joints):
      force = state_info.collision_force(joint, pos, vel, movent_quantity)
      m_steering = state_info.joints[joint]["position"]

      if force is not None:
        steer(joint, m_steering, self.sphereUid, force)

  def _current_joint_positions(self, joints):
    
    #position, velocity, reaction_forces, motor_torque = positions[0]
    return [p.getJointState(self.sphereUid,pos)[0] for pos in joints]

  def get_xy_coord(self):
    x_positions = []
    y_positions = []
    for joint in self.state_info.elastic_joints.horizontal:
      y, x, z = p.getLinkState(self.sphereUid, joint, 1)[0]
      x_positions.append(x)
      y_positions.append(y)
    x = np.array(x_positions).mean()
    y = np.array(y_positions).mean()
    return x, y

  def _x(self):
    x, y = self.get_xy_coord()
    return x

  def to_state(self, joints=None):
    # force on joko 0 tai 1 tai 2
    #forces_indices = self.state_info.forces_indices()
    #forces_indices = np.array(forces_indices) / self.state_info.max_force_index
    if joints is None:
      joints = self.state_info.elastic_joints.horizontal

    positions = self._current_joint_positions(joints)
    positions = np.array(positions)
    #positions = (np.array(positions)+self.state_info.movent_quantity)/(self.state_info.movent_quantity*2)

    velocity_unit = 6
    velocities = [(p.getJointState(self.sphereUid,pos)[1] + velocity_unit) / (2*velocity_unit) for pos in joints]
    if self.number_of_inp_vars == 2:
      inp = positions.tolist() + velocities
    elif self.number_of_inp_vars == 1:
      inp = positions.tolist()
    else:
      raise Exception("illegal number_of_inp_vars")
    
    return inp

  def _head_and_tail_coords(self):
    xat = []
    yat = []
    y_b, x_b, z_b = p.getBasePositionAndOrientation(self.sphereUid)[0]
    xat.append(x_b)
    yat.append(y_b)

    for i in range(p.getNumJoints(self.sphereUid)):
      y, x, z = p.getLinkState(self.sphereUid, i, 1)[0]
      xat.append(x)
      yat.append(y)

    #x1, x2 = (x_b + xat[5]) / 2, (xat[-1] + xat[-7]) / 2
    #y1, y2 = (y_b + yat[5]) / 2, (yat[-1] + yat[-7]) / 2

    assert len(xat) > 2

    half_way = len(xat)//2
    x1, x2 = np.array(xat[:half_way]).mean(), np.array(xat[half_way:]).mean()
    y1, y2 = np.array(yat[:half_way]).mean(), np.array(yat[half_way:]).mean()

    return (x1, y1), (x2, y2)


  def _distance_along_line(self, old_head_xy, old_tail_xy, new_center, old_center):
    new_center = Point(new_center)
    reward_line = LineString([old_head_xy, old_tail_xy])

    x = np.array(new_center.coords[0])

    u = np.array(reward_line.coords[0])
    v = np.array(reward_line.coords[len(reward_line.coords)-1])

    n = v - u
    n /= np.linalg.norm(n, 2)

    P = u + n*np.dot(x - u, n)
    P_point = Point(P)
    distance = P_point.distance(Point(old_center))
    distance = self._get_sign(old_center, old_head_xy, P)*distance

    return distance

  def _get_sign(self, center, head, projected):
    """
    if worm moved forward, then reward is positive, if backward, then negative.
    """
    if np.sign(head[0]-center[0]) == np.sign(projected[0]-center[0]) and np.sign(head[1]-center[1]) == np.sign(projected[1]-center[1]):
      return 1
    else:
      return -1

  def _get_center_point(self, head, tail):
    x = (head[0] + tail[0])/2
    y = (head[1] + tail[1])/2
    return (x, y)

  def _distance_from_line(self, old_head_xy, old_tail_xy, new_center):
    p1 = np.array(old_head_xy)
    p2 = np.array(old_tail_xy)
    p3 = np.array(new_center)
    d = np.linalg.norm(np.cross(p1-p2, p2-p3))/np.linalg.norm(p1-p2)
    return d

  def _calculate_distance(self, old_head_xy, old_tail_xy, new_head_xy, new_tail_xy):
    
    old_center = self._get_center_point(old_head_xy, old_tail_xy)
    new_center = self._get_center_point(new_head_xy, new_tail_xy)

    distance = self._distance_along_line(old_head_xy, old_tail_xy, new_center, old_center)
    from_line = self._distance_from_line(old_head_xy, old_tail_xy, new_center)
    # TODO: eihän from_line ole koskaan negatiivinen?
    # TODO: vähennä returnista pieni rn luku / distancella. Tarkoitus antaa penaltyä, jos pysyy paikallaan.
    return distance



  def move_camera(self, keyboard_actions):
    outp = p.getDebugVisualizerCamera()
    yaw = outp[8]
    pitch = outp[9]
    dist = outp[10]
    target = list(outp[11])

    if keyboard_actions.right():
      target[0] = target[0] + 1
    if keyboard_actions.left():
      target[0] = target[0] - 1
    if keyboard_actions.up():
      target[1] = target[1] + 1
    if keyboard_actions.down():
      target[1] = target[1] - 1
      
    p.resetDebugVisualizerCamera(dist,yaw,pitch,target)