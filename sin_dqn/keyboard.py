import pybullet as p

class KeyboardActions:
  def __init__(self):
    self.actions = []
    self.right_arrow = "RIGHT_ARROW"
    self.left_arrow = "LEFT_ARROW"
    self.up_arrow = "UP_ARROW"
    self.down_arrow = "DOWN_ARROW"
    self.z_char = "z"
    self.x_char = "x"
    self.q_char = "q"
    self.a_char = "a"


  def right(self):
    return self.fetch_action(self.right_arrow)

  def left(self):
    return self.fetch_action(self.left_arrow)

  def up(self):
    return self.fetch_action(self.up_arrow)

  def down(self):
    return self.fetch_action(self.down_arrow)

  def z(self):
    return self.fetch_action(self.z_char)

  def x(self):
    return self.fetch_action(self.x_char)

  def q(self):
    return self.fetch_action(self.q_char)
  
  def a(self):
    return self.fetch_action(self.a_char)

  def _update_actions(self):
    keys = p.getKeyboardEvents()
    for k, v in keys.items():
      if (k == p.B3G_RIGHT_ARROW and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.right_arrow)
      if (k == p.B3G_LEFT_ARROW and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.left_arrow)
      if (k == p.B3G_UP_ARROW and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.up_arrow)
      if (k == p.B3G_DOWN_ARROW and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.down_arrow)

      # 122 & 120 = z ja x
      if (k == 122 and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.z_char)
      if (k == 120 and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.x_char)

      # 113 & 97 = q ja a
      if (k == 113 and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.q_char)
      if (k == 97 and (v & p.KEY_WAS_TRIGGERED)):
        self.actions.append(self.a_char)


  def fetch_action(self, action):
    self._update_actions()
    if action in self.actions:
      self.actions.remove(action)
      return True
    else:
      return False