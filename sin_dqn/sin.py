# ehkä pitäisi tehdä A2C:llä: https://github.com/ShangtongZhang/DeepRL
# https://medium.com/@unnatsingh/deep-q-network-with-pytorch-d1ca6f40bfda
import math
import random
import numpy as np
#import matplotlib
#import matplotlib.pyplot as plt
from collections import namedtuple
from itertools import count
#from PIL import Image
import pybullet as p
from dqn_util import load_model, save_model



"""

Mato on hyvä etenemään 2d pinnalla mutta erittäin kömpelö 3d pinnalla
* koita säätää kamera madon päähän.
* koita saada mato näkemään kapea keila edessään
* koita opettaa mato tunnistamaan aukotedessä
** pystyy ohjaamaan etummaisinta niveltä kohti aukkoa.
** kuvasta voisi tunnistaa mikä suunta (ylös,alas,oikea,vasen, keskellä)
** etummainen nivel toimii samojen sin käskyjen mukaan kuin muutkin nivelet, mutta sen lisäksi saa ohjeita myös aukkojen tunnistus ohjelmalta
*** aukkojentunnistus ohjelma saa inputiksi seuraavan sinin? Ei välttämättä tarvitse saada.





tee seuraavaksi:
lisää uusi komponentti käärmeen ekan nivelen ohailuun. oikeasti eka nivel on kaksi niveltä (ehkä 1 ja 2)
DQN malli ennustaa mikä 5 actionista pitäisi ottaa. 
* Input: nivel kulmat, viimeisin reward(on käytännössä paljon edennyt). state_size = 10 + 1
* output: action [0,4]
- etsii siis hyvän strategian millä heilutella ensimmäistä



Ratkaistavia asioita:
* Kuinka käärmeen pää osaa tehdä oikeat manoverit?
** Esim nostaa ylöspäin jos matala este tai suunnistaa oikealla/vasemmalle jos este.
** Älä yritä automatisoida tätä nyt. Tee näppäimet joilla voi auttaa pään liikkeissä. Esim näppäimet "wasd". 

* TÄMÄ: kuinka käärmeen vartalo osaa reagoida esteisiin.
** esim niin tiukka kohta, ettei oikein pysty luikertelemaan. Jos kuitenkin nostaisi kroppaa vähän niin luikertelu olisikin helpompaa.
** kaksi tiukkaa kohtaa. ei oikein osaa luikerralla. Pitäisikö soittaa aallonpituus tapauskohtaisesti?
* jossain kohdin olisi hyvä pysähtyä ja jopa pakittaa
** esim jos ekat 3 links jääneet jumiin koloon tai nurkkaan.

Ratkaisun avaimia:
* Käärme haluaa tehdä korkean aallon jolla pääsee esteen päälle
* Voisi haluta tehdä vain yhden aallon. jolla nousta kyseisen esteen päälle -> tarvitsee jonkinlaisen profiilin millainen käärmeen fyysinen ympäristö on.
** kuinka edetä alustalla. Pitäisikö yrittää mallintaa ympäristöä? Mitkä nivelen pisteet kokee vastavoimaa? getContactPoints
*** contact points piirtää kuvan ympäristostä. contact points ei ole saatavissa oikeasta robostista.
*** ehkä liikuttu tila luo ympäristön muodon.esimerkiksi jokaisen nivelen 3D liikeet on tila. Miten vertical?






Tämän jälkeen siirry rakentamaan actual robottia.



"""




import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import torch
import torch.optim as optim
#from model import ActorCritic
from time import gmtime, strftime

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
import torch
import torch.optim as optim
#from model import ActorCritic 
import numpy as np

from keyboard import KeyboardActions
from dqn_util import ReplayMemory, DQN, Transition

# TODO: BATCH_SIZE = 128
BATCH_SIZE = 128
# n-step toteutuksessa gamma pitäisi kai olla aina 1.
GAMMA = 0.5
#model_name = "sin2_20200304155405_0.49"
ADD_DISTURBTION = True
model_name = "sin1_20200308163249_-1.5"
TESTIN_LEARNED_PARAMS = False
if TESTIN_LEARNED_PARAMS:
  load_from_model = True
else: 
  load_from_model = False

has_eps = not load_from_model
if has_eps:
  EPS_START = 0.9
  EPS_END = 0.05
  EPS_DECAY = 250
else:
  EPS_START = 0.0
  EPS_END = 0.0
  EPS_DECAY = 1
TARGET_UPDATE = 100
lr = 0.0001
memory_size = 500
alfa_R = 1/(memory_size*10)

#2144 iteraationkaan jälkeen ei oppinut. Oppi hetkellisesti ennen 2k iter, mutta unohti.
def get_batch(memory):
  transitions = memory.sample(BATCH_SIZE)
  # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
  # detailed explanation). This converts batch-array of Transitions
  # to Transition of batch-arrays.
  return Transition(*zip(*transitions))

def get_eps(steps_done):
  eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
  return eps_threshold

def compute_returns(rewards, G_avg, next_state):
  G = 0
  for reward in range(len(rewards)):
    G += reward - G_avg
  return  G + next_state

def optimize_model(memory, device, policy_net, env, target_net, optimizer):
  def to_val(tensor):
    return tensor.cpu().detach().numpy()
  global TESTIN_LEARNED_PARAMS
  if len(memory) < BATCH_SIZE:
    return 0
  
  batch = get_batch(memory)
  next_states = torch.cat([s for s in batch.next_state if s is not None]).view(-1, env.state_size)

  state_batch = torch.cat(batch.state).view(-1, env.state_size)
  action_batch = torch.cat(batch.action)

  reward_batch = torch.cat(batch.reward)
  state_action_values = policy_net(state_batch)


  state_action_values = state_action_values.gather(1, action_batch.unsqueeze(1)).squeeze()

  nsv = target_net(next_states)
  next_state_values = nsv.max(1)[0].detach()
  
  expected_state_action_values = (next_state_values * GAMMA) + reward_batch
  target = expected_state_action_values.sum()
  output = state_action_values.squeeze().sum()

  batch_advantage = target - output
  batch_advantage = batch_advantage.cpu().detach().numpy()
  loss = F.smooth_l1_loss(output, target)
  
  # Optimize the model
  optimizer.zero_grad()
  loss.backward()

  for param in policy_net.parameters():
    param.grad.data.clamp_(-1, 1)
  if not TESTIN_LEARNED_PARAMS:
    optimizer.step()

  debug_msg = f"target {to_val(target)}, output {to_val(output)}, next_state_value {to_val(next_state_values[-1])}, rew_val {to_val(reward_batch[-1])} "

  return batch_advantage, loss.cpu().detach().numpy(), debug_msg

def get_state(env, reward):
  state = env._current_joint_positions(env.state_info.elastic_joints.horizontal)
  state.append(reward)
  return state



def run_method(env, save_checkpoints = True, checkpoint_freq=50, version="perftest"):
  take_pic = False
  if take_pic:
    from PIL import Image
    ima = np.array(p.getCameraImage(1000,1000)[2])
    image = Image.fromarray(ima)
    file_path = "your_file.jpeg"
    rgb_im = image.convert('RGB')
    rgb_im.save('audacious.jpg')
    print("image saved")

  set_camera = False
  if set_camera:
    # On suht hankala. 
    # * Pitää aina etsiä ensimmäisen palan kärjen sijainti. Ehkä helpoin jos tekee siitä superohuen ja ottaa vaan keskipisteen. 
    # * sitten pitää päätellä 
    # ** yaw: missä kulmassa palikka on positiivisen y-akseliin nähden. Eli menosuunta on 0.
    # ** pitch: mikä on kulma. Niin kauan kuin mato on vain vaakatasossa niin pitch = 0. Vertical muutokset vaikuttaa pitchiin. getBasePositionAndOrientation ja getLinkState
    # getEulerFromQuaternion
    p.resetDebugVisualizerCamera(5,0,-45,[10,10,10])

  batch_advantage = 0
  steps_done = 0

  # if gpu is to be used
  device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
  print(device)
  env.reset()

  # TRAINING:
  n_actions = env.action_space
  policy_net = DQN(env.state_size, n_actions).to(device)
  target_net = DQN(env.state_size, n_actions).to(device)
  target_net.load_state_dict(policy_net.state_dict())
  target_net.eval()

  #optimizer = optim.RMSprop(policy_net.parameters(), lr=lr, momentum=0.9)
  optimizer = optim.SGD(policy_net.parameters(), lr=lr, momentum=0.9)
  
  optimizer, policy_net = load_model(model_name, policy_net, optimizer, load_old_params=load_from_model)

  memory = ReplayMemory(memory_size)

  STEPS_PER_STATE = 15
  G_avg = 0
  R_avg = 0
  R_pure_avg = 0
  batch_advantage = 0
  # ---TRAINING---
  sum_loss = 0
  num_episodes = 500000
  debug = False

  #kokeile 
  #pybullet.resetDebugVisualizerCamera() 

  def step(prog, dist, z_delay, z_phase, abs_prog, action, keyboard_actions):
    env.move_camera(keyboard_actions)
    #prog = abs_prog if action.cpu().detach().numpy()[0] > 0 else -abs_prog

    reardw_hist = []

    for t in count():
      #for i, joint in enumerate(env.state_info.elastic_joints):
      state, reward = env.render(env.state_info.elastic_joints.horizontal, env.state_info.elastic_joints.vertical, prog, dist, z_delay, z_phase, action, keyboard_actions)
      if reward is None:
        continue
      
      #R_avg = (1-alfa_R)*R_avg + alfa_R*reward
      reardw_hist.append(reward)
      if t > STEPS_PER_STATE:
        rew_avg = np.array(reardw_hist).mean()

        R_avg_str = str(np.round(rew_avg, 2))
        latest_R_str = str(np.round(reward, 2))
        z_delay_str = str(np.round(z_delay, 2))
        z_phase_str = str(np.round(z_phase, 2))

        msg = f"R_avg {R_avg_str}, z-del {z_delay_str}, z-ph {z_phase_str}"
        return rew_avg, msg

  # vaihda prog ja dist
  prog = 0.22
  dist = 12
  #dists = [8,12]
  z_delay, z_phase = 1.3, 2.38
  smothed_loss = []
  reward = 0
  rewards = []
  keyboard_actions= KeyboardActions()

  for i_episode in range(num_episodes):
    state = get_state(env, reward)
    action, eps_threshold, action_source = select_action(state, n_actions, device, policy_net, steps_done)

    #dist = dists[action.cpu().detach().numpy()[0]]
    # TODO: 
    steps_done += 1

    reward, msg = step(prog, dist, z_delay, z_phase, prog, 0, keyboard_actions)
    #reward, msg = step(prog, dist, z_delay, z_phase, prog, action.cpu()[0].item())
    rewards.append(reward)
    if len(rewards) > 5:
      rewards = rewards[1:]
    R_avg += reward
    # TODO: next_sate on hankala määrittää tässä tapauksessa.
    next_state = torch.tensor(get_state(env, reward)).float().to(device) 
    next_state_val = policy_net(next_state)
    next_state_val = next_state_val.max(0)[0].detach().item()
    G = compute_returns(rewards, G_avg, next_state_val)
    mem_state = torch.tensor(state).float().to(device)
    mem_next_state = next_state
    #mem_action = torch.tensor(action).to(device)
    #mem_target = torch.tensor([target]).to(device)
    mem_target = torch.tensor([G]).to(device)
    memory.push(mem_state, action, mem_next_state, mem_target)
    #step(prog, dist, z_delay, z_phase)

    if i_episode > BATCH_SIZE:
      batch_advantage, loss, debug_msg = optimize_model(memory, device, policy_net, env, target_net, optimizer)
      smothed_loss.append(loss)
      G_avg_str = str(np.round(G_avg, 2))
      G_avg = G_avg + batch_advantage*alfa_R
      eps_threshold_str = str(np.round(eps_threshold, 2))
      loss_str = str(np.round(np.array(smothed_loss).mean(), 2))
      R_avg_str= str(np.round(R_avg, 2))
      if len(smothed_loss) > 15:
        smothed_loss = smothed_loss[1:]
      if i_episode % 10 == 0:
        print(f"{i_episode}|{action_source}, loss {loss_str}, {msg}, G_avg_str {G_avg_str}, eps {eps_threshold_str}, action {action.cpu().detach().numpy()}, R_sum {R_avg_str}")
      if debug:
        print(debug_msg)
    else:
      if i_episode % 10 == 0:
        print("i_episode", i_episode)
    
    if i_episode % TARGET_UPDATE == 0:
      print("policy_net -> target_net")
      target_net.load_state_dict(policy_net.state_dict())
    

def select_action(state, n_actions, device, policy_net, steps_done):
  eps_threshold = get_eps(steps_done)
  sample = random.random()
  if sample > eps_threshold:
    with torch.no_grad():
      # t.max(1) will return largest column value of each row.
      # second column on max result is index of where max element was
      # found, so we pick action with the larger expected reward.
      action_values = policy_net(torch.tensor(state).float().to(device).unsqueeze(0))
      pol_acts = action_values.max(1)[1].view(1, -1)
      return pol_acts[0], eps_threshold, "model"

  else:
    eps_acts = [random.randrange(n_actions)]
    return torch.tensor(eps_acts, device=device, dtype=torch.long), eps_threshold, "random"

