import torch
from env_utils import EnvParams
import pybullet as p

from snake_util import steer, TurnCorner
import time
import math
import numpy as np

from sin import run_method
import argparse


"""
1. tee uusi folder tai "subproject". Kopio sin_grid pohjaksi
2. settaa 3 muuttujaa vakioksi. 
3. input on 2 kokoinen onehot. [1,0] mene eteenpäin, [0,1] mene taaksepäin
4. Output on 2 dist actionia. Jos action1 niin dist = 2.3 jos action0 niin -2,3
Jos toimii tähän asti niin todistettu, että yksinkertainen solution toimii

tee ensimmäinen fyysinen malli

5. tee toinen malli jossa prog output.
* prog vaikuttaa miten isoa aaltoa käärme tekee
6. mallin input on nivel kulmat ja nivelkulmien lähihistoria
7. output on 2 actionia.  toinen on hyvä avoimella ja toinen huono (liian pientä aaltoa)
Jos oppii valitsemaan hyvän, niin todistettu, että osaa päätellä nivelkulmista prog, helpossa tapauksessa
8. Tee esterata.
9. etsi käsin joku esteradalla hyvin toimiva dist
10 . tee training jossa satunnaisesti esteradalla tai avoimella. 
Jos oppii tämän niin todistettu, että sisäinen malli osaa ainakin jossain määrin sovittaa luikertelutyylinsä maasto-olosuhteisiin.




Tee toiminnallisuus joka kääntää ylösalaisin olevan käärmeen oikeinpäin.
Jos on huonoa maastoa niin 90-astetta käännetty käärme saattaa toimia aika hyvin.
* Käärme erittäin pehmeälle maastolle.
* hepompi havaita kuin matalampi käärme.
Pitäisi refactoroida koodi. Rupeaa olemaan kaikkea sikin sokin.


Agility:
Sokkelossa menemisestä pitää tehdä sulavampaa kuin sin aalto. Ehkä pitäisi olla erilaisia sin aaltoja pitkin käärmettä riippuen liikkumatilatasta.
* jokaiselle nivelelle inputtina 3 lähimmäistä niveltä. output ennustaa mikä tämän sin aallon amplitudin pitäisi olla. Eli aallon pituus olisi sama? 
  Mitä jos aallonpituuskin voisi vaihdella?
TEE TÄMÄ: Jos tunnistaa jumikohdat niin voisi yrittää niihin jotain kikkoja. Eli ei tarvitse viedä esim "stuck" behaviouria koko käärmeen läpi.
* ota inputiksi 5 pisteen nivelkulmat. Ennusta pitääkö keskimmäiselle antaa stuck-signaali?
jos pahasti jumissa niin voisi koittaa pakittaa ja sitten taas eteenpäin.




"""

class SinEnv(EnvParams):
  def __init__(self):
    super(SinEnv, self).__init__()
    self.ts = 0
    self.prev_pos = {}
    self.turn_corner = None

  def _target_position(self, joint, ts, ts_progress, joint_dist):
    return math.sin((ts*ts_progress+(joint)*math.pi/joint_dist))*self.state_info.movent_quantity_hor

  def _cos_position(self, joint, ts, ts_progress, joint_dist):
    return math.cos((ts*ts_progress+(joint)*math.pi/joint_dist))*self.state_info.movent_quantity_hor

  def driver_actions(self, joint, joint_ver, action, keyboard_actions):
    if keyboard_actions.z():
      self.turn_corner = TurnCorner(TurnCorner.left, self.state_info).to_left_and_right()
    if keyboard_actions.x():
      self.turn_corner = TurnCorner(TurnCorner.right, self.state_info).to_left_and_right()
    if keyboard_actions.q():
      self.turn_corner = TurnCorner(TurnCorner.stuck, self.state_info).to_stuck()

    if self.turn_corner is None:
      return 0,0, TurnCorner.no_turn
    else:
      #direction = self.turn_corner.direction
      action_hor, action_ver, action_name = self.turn_corner.get_action(joint)
      if action_hor is None:
        action_hor = 0
        action_ver = 0
        self.turn_corner = None

      return action_hor, action_ver, action_name

  def _vertical_force_position(self, sin_action, prev, add_ver, turn):
    vertical_force = 30
    if turn == TurnCorner.left and  sin_action < prev: 
      return vertical_force, add_ver
    elif turn == TurnCorner.right and  sin_action > prev:
      return vertical_force, add_ver
    elif turn == TurnCorner.stuck:
      return vertical_force*5, add_ver
    else:
      # normal case, going forward
      if abs(sin_action) < abs(prev):
        return vertical_force, self.state_info.move_forw_ver + add_ver
      else:
        return 0, 0.0

  # TODO: keyboard_actions should be part of actions.
  def render(self, joints_hor, joints_ver, ts_progress, joint_dist, delay, phase, action, keyboard_actions):
    #import time
    #time.sleep(3)
    
    for joint, joint_ver in zip(np.array(joints_hor), np.array(joints_ver)):
      add_hor, add_ver, turn = self.driver_actions(joint, joint_ver, action, keyboard_actions)
      if joint not in self.prev_pos:
        self.prev_pos[joint] = [0]
      
      prev = self.prev_pos[joint][0]
      sin_action = self._target_position(joint, self.ts, ts_progress, joint_dist)

      if joint == 1:
        sin_action = self._cos_position(joint, self.ts, ts_progress, joint_dist)*-1

        # Näiden ongelma on, että pitää tietää world koordinaatti, mitä ei oikeasti tieddetä.
        # local koordinaatti saattaisi onnistuakkin?
        #linpos = p.getLinkState(self.sphereUid, 1)[0]
        #linor = p.getLinkState(self.sphereUid, 1)[1]
        #pitch,roll,yaw = p.getEulerFromQuaternion(linor)

        
      self.prev_pos[joint].append(sin_action)
      self.prev_pos[joint] = self.prev_pos[joint][1:]



      # *** Vertical movement starts***
      force, m_steering = self._vertical_force_position(sin_action, prev, add_ver, turn)
      self.state_info.set_joint_status(joint_ver, m_steering, force)
      steer(joint_ver, m_steering, self.sphereUid, force)

      
      # *** Horizonal movement ends***
      m_steering, force = self.state_info.action_to_position_and_force(sin_action, self.state_info.movent_quantity_hor)
      m_steering += add_hor
      force = force
      self.state_info.set_joint_status(joint, m_steering, force)
      steer(joint, m_steering, self.sphereUid, force)

    self.ts += 1

    reward = self._reward()

    for forward in range(self.timesteps_per_render):
      self._enforce_constraints()
      p.stepSimulation()

    state = self.to_state()
    #positions = self._current_joint_positions()

    return state, reward


env_util = SinEnv()

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-t','--terminal', type=str)

args = parser.parse_args()


#env_util = EnvParams()
run_method(env_util, version="sin"+args.terminal, save_checkpoints = True)


