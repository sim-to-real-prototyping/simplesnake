import pybullet as p
import time
import math
from state_info import StateInfo
import numpy as np
import random

def steer(joint, targetPos, sphereUid, force = 20):
  p.setJointMotorControl2(sphereUid,
                        joint,
                        p.POSITION_CONTROL,
                        targetPosition=targetPos,
                        force=force)

def create_snake():
  sphereRadius = 0.25
  sphereRadiusa = 0.01
  sphereRadius1 = 0.25
  sphereRadius2 = 0.25
  #colBoxId = p.createCollisionShapeArray([p.GEOM_BOX, p.GEOM_SPHERE],radii=[sphereRadius+0.03,sphereRadius+0.03], halfExtents=[[sphereRadius,sphereRadius,sphereRadius],[sphereRadius,sphereRadius,sphereRadius]])
  colBoxId = p.createCollisionShape(p.GEOM_BOX,halfExtents=[sphereRadius1, sphereRadius, sphereRadius2])
  colBoxId2 = p.createCollisionShape(p.GEOM_BOX,halfExtents=[sphereRadius1, sphereRadiusa, sphereRadius2])
  colBoxId3 = p.createCollisionShape(p.GEOM_BOX,halfExtents=[sphereRadius1, sphereRadiusa, sphereRadius2])

  mass = 1
  visualShapeId = -1
  state_info = StateInfo()

  link_Masses = []
  linkCollisionShapeIndices = []
  linkVisualShapeIndices = []
  linkPositions = []
  linkOrientations = []
  linkInertialFramePositions = []
  linkInertialFrameOrientations = []
  indices = []
  jointTypes = []
  axis = []

  for block in range(20):
    # Ensimmäinen solid osa on itsenäinen multibodyssä
    # ensimmäinen nivel osa
    joints_per_section=4
    i=block*joints_per_section + 0
    link_Masses.append(0.1)
    linkCollisionShapeIndices.append(colBoxId2)
    linkVisualShapeIndices.append(-1)
    dist = (sphereRadiusa/2 + sphereRadius / 2)*2
    linkPositions.append([0, dist + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_FIXED)
    axis.append([0, 0, 0])
    # Toinen nivelen osa
    i=block*joints_per_section + 1
    link_Masses.append(0.1)
    linkCollisionShapeIndices.append(colBoxId2)
    linkVisualShapeIndices.append(-1)
    linkPositions.append([0, sphereRadiusa * 2.0 + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_REVOLUTE)
    axis.append([0, 0, 1])
    state_info.add_elastic_joint_hor(i)

    # vertical joint
    i=block*joints_per_section + 2
    link_Masses.append(0.1)
    linkCollisionShapeIndices.append(colBoxId3)
    linkVisualShapeIndices.append(-1)
    linkPositions.append([0, sphereRadiusa * 2.0 + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_REVOLUTE)
    axis.append([1, 0, 0])
    
    state_info.add_elastic_joint_ver(i)

    # toinen solid osa
    i=block*joints_per_section + 3
    link_Masses.append(1)
    linkCollisionShapeIndices.append(colBoxId)
    linkVisualShapeIndices.append(-1)
    dist = (sphereRadiusa/2 + sphereRadius / 2)*2
    linkPositions.append([0, dist + 0.01, 0])
    linkOrientations.append([0, 0, 0, 1])
    linkInertialFramePositions.append([0,0,0])
    linkInertialFrameOrientations.append([0,0, 0,1])
    indices.append(i)
    jointTypes.append(p.JOINT_FIXED)
    axis.append([0, 0, 0])

  basePosition = [0, -1, sphereRadius2]
  baseOrientation = [0, 0, 1, 0]
  sphereUid = p.createMultiBody(mass,
                                colBoxId,
                                visualShapeId,
                                basePosition,
                                baseOrientation,
                                linkMasses=link_Masses,
                                linkCollisionShapeIndices=linkCollisionShapeIndices,
                                linkVisualShapeIndices=linkVisualShapeIndices,
                                linkPositions=linkPositions,
                                linkOrientations=linkOrientations,
                                linkInertialFramePositions=linkInertialFramePositions,
                                linkInertialFrameOrientations=linkInertialFrameOrientations,
                                linkParentIndices=indices,
                                linkJointTypes=jointTypes,
                                linkJointAxis=axis)

  p.setGravity(0, 0, -9.81)
  p.setRealTimeSimulation(0)

  anistropicFriction = [0.05, 0.1, 0.1]
  p.changeDynamics(sphereUid, -1, lateralFriction=2, anisotropicFriction=anistropicFriction)
  for i in range(p.getNumJoints(sphereUid)):
    p.getJointInfo(sphereUid, i)
    p.changeDynamics(sphereUid, i, lateralFriction=2, anisotropicFriction=anistropicFriction)

  p.enableJointForceTorqueSensor(sphereUid, 0)

  for joint in state_info.elastic_joints.horizontal:
    p.enableJointForceTorqueSensor(sphereUid, joint, True)

  return sphereUid, state_info

def create_track():
  def create_block(length, width, height, basePosition,mass):
    colBoxId = p.createCollisionShape(p.GEOM_BOX,halfExtents=[width, length, height])

    visualShapeId = -1
    state_info = StateInfo()

    baseOrientation = [0, 0, 1, 0]
    block_id = p.createMultiBody( mass,
                                  colBoxId,
                                  visualShapeId,
                                  basePosition,
                                  baseOrientation,
                                  linkMasses=[],
                                  linkCollisionShapeIndices=[],
                                  linkVisualShapeIndices=[],
                                  linkPositions=[],
                                  linkOrientations=[],
                                  linkInertialFramePositions=[],
                                  linkInertialFrameOrientations=[],
                                  linkParentIndices=[],
                                  linkJointTypes=[],
                                  linkJointAxis=[])

    p.setGravity(0, 0, -9.81)
    p.setRealTimeSimulation(0)

    anistropicFriction = [1, 1, 1]
    p.changeDynamics(block_id, -1, lateralFriction=2, anisotropicFriction=anistropicFriction)
    for i in range(p.getNumJoints(block_id)):
      p.getJointInfo(block_id, i)
      p.changeDynamics(block_id, i, lateralFriction=2, anisotropicFriction=anistropicFriction)

    return block_id, state_info
  
  def get_obstacle(id, dist_from_start):
    from_center = random.random()
    from_center = from_center - 0.5
    from_center = from_center*0.7
    if id == 0:
      return create_block(0.2, 0.2, 0.2, [from_center, dist_from_start, 0.2], 1000)
    elif id == 1:
      return create_block(1.5, 0.3, 0.2, [from_center, dist_from_start, 0.2], 1)
    elif id == 2:
      return create_block(0.2, 0.4, 0.2, [from_center, dist_from_start, 0.2], 1000)
    else:
      raise Exception("illegal id")

  track_len = 500
  width1 = 0.25
  height = 1.25
  # Create track
  block1 = create_block(track_len, width1, height, [0.9, track_len-10, height], 5000)
  block2 = create_block(track_len, width1, height, [-0.9, track_len-10, height], 5000)
  blocks = [block1, block2]

  # create obstacles
  obst_dist = 4
  for i in range(track_len // obst_dist):
    id = np.random.randint(3, size=1)[0]
    dist = np.random.randint(obst_dist -2, size=1)[0] + 1
    dist= i*obst_dist+dist
    blocks.append(get_obstacle(id, dist))

  return blocks

class TurnCorner:
  left = "left"
  right = "right"
  stuck = "stuck"
  no_turn = "no_turn"
  def __init__(self, direction, state_info):
    self.state_info = state_info
    self.direction = direction
  
  def _turn_behaviour(self, neighs_to_incl, wave_len):

    self.joints = []
    neighs_to_incl = 1
    for i, joint in enumerate(self.state_info.elastic_joints.horizontal):
      for _ in range(wave_len):
        following = self.state_info.elastic_joints.horizontal[i:i+neighs_to_incl+2]
        if len(following) < neighs_to_incl + 1:
          break
        self.joints.append(following)

  def _left_or_right_action(self):
    vertical_angle = -0.4
    return (-0.5, vertical_angle, "left") if self.direction == "left" else (0.5, vertical_angle, "right")

  def to_left_and_right(self):
    self._turn_behaviour(2,10)
    self._get_action = self._left_or_right_action
    return self

  def _to_stuck(self):
    return 0, 0.4, "stuck"

  def to_stuck(self):
    self._turn_behaviour(2, 20)
    self._get_action = self._to_stuck
    return self

  def get_action(self, joint):
    if self.joints is None:
      return None, None, None

    curr_actions = self.joints[0]
    if joint not in curr_actions:
      return 0,0, "no_turn"

    else:
      curr_actions.remove(joint)
      if curr_actions == []:
        self.joints = self.joints[1:]
      else:
        self.joints[0] = curr_actions
      if len(self.joints) == 0:
        self.joints = None

      return self._get_action()
