# ehkä pitäisi tehdä A2C:llä: https://github.com/ShangtongZhang/DeepRL
# https://medium.com/@unnatsingh/deep-q-network-with-pytorch-d1ca6f40bfda
import math
import random
import numpy as np
#import matplotlib
#import matplotlib.pyplot as plt
from collections import namedtuple
from itertools import count
#from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import torch
import torch.optim as optim
#from model import ActorCritic
import numpy as np
from time import gmtime, strftime
from collections import namedtuple



BATCH_SIZE = 128
# n-step toteutuksessa gamma pitäisi kai olla aina 1.
GAMMA = 0.99
#model_name = "sin2_20200304155405_0.49"
model_name = "sin1_20200308163249_-1.5"
TESTIN_LEARNED_PARAMS = True
if TESTIN_LEARNED_PARAMS:
  load_from_model = True
else: 
  load_from_model = False

has_eps = not load_from_model
if has_eps:
  EPS_START = 0.9
  EPS_END = 0.05
  EPS_DECAY = 25000
else:
  EPS_START = 0.0
  EPS_END = 0.0
  EPS_DECAY = 1

TARGET_UPDATE = 30
lr = 0.0001
memory_size = 500
alfa_R = 1/memory_size

def get_eps(steps_done):
  eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
  return eps_threshold

def select_action(state, n_actions, device, policy_net, steps_done, joints_cnt):
  eps_threshold = get_eps(steps_done)
  sample = random.random()
  if sample > eps_threshold:
    return action

  else:
    #eps_acts = [[random.randrange(n_actions)]]
    eps_acts = [random.randrange(n_actions)]

    return torch.tensor(eps_acts, device=device, dtype=torch.long), random.random()*2-1

def compute_returns(rewards, G_avg):
  returns = []
  G = 0
  for step in reversed(range(len(rewards))):
    G = (rewards[step] - G_avg) + G

  return G

# t1 friction 2 kertainen
# t2, force on 2 kertainen
def run_method(env, save_checkpoints = True, checkpoint_freq=50, version="perftest"):
  # LEARNABLE PARAMETERS
  # 0.1, 8 R 0.23
  #ts_progress = 0.1
  #joint_dist = 8
  ts_progress_str = "ts_progress"
  joint_dist_str = "joint_dist"
  prev_var = ts_progress_str

  steps_done = 0
  
  episode_durations = []
  joint_count = len(env.state_info.elastic_joints)


  # if gpu is to be used
  device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
  print(device)
  env.reset()

  # TRAINING:

  
  G_avg = 0
  R_avg = 0
  R_pure_avg = 0
  
  batch_rew = 0

  # ---TRAINING---
  sum_loss = 0
  num_episodes = 500000

  #kokeile 
  #pybullet.resetDebugVisualizerCamera() 
  """
  while (1):
      cubePos, cubeOrn = p.getBasePositionAndOrientation(dog)
      p.resetDebugVisualizerCamera( cameraDistance=cdist, cameraYaw=cyaw, cameraPitch=cpitch, cameraTargetPosition=cubePos)

      keys = p.getKeyboardEvents()
      #Keys to change camera
      if keys.get(100):  #D
          cyaw+=1
      if keys.get(97):   #A
          cyaw-=1
      if keys.get(99):   #C
          cpitch+=1
      if keys.get(102):  #F
          cpitch-=1
      if keys.get(122):  #Z
          cdist+=.01
      if keys.get(120):  #X
          cdist-=.01
  """

  def step(prog, dist, z_delay, z_phase):
    reardw_hist = []

    for t in count():
      #for i, joint in enumerate(env.state_info.elastic_joints):
      state, reward = env.render(env.state_info.elastic_joints.horizontal, env.state_info.elastic_joints.vertical, prog, dist, z_delay, z_phase)
      if reward is None:
        continue
      
      #R_avg = (1-alfa_R)*R_avg + alfa_R*reward
      reardw_hist.append(reward)
      if t > 2500:
        rew_avg = np.array(reardw_hist).mean()

        R_avg_str = str(np.round(rew_avg, 2))
        latest_R_str = str(np.round(reward, 2))

        print(f"R_avg {R_avg_str}, {z_delay}, {z_phase}")
        return

  env.reset()

  direction = "forw"
  if direction == "forw":
    prog = 0.22
    #prog = 0.0
  elif direction == "backw":
    prog = -0.22

 
  #prog = -0.22
  dist = 12
  
  for z_delay in np.linspace(1.8, 3.0, num=4):
    for z_phase in np.linspace(1.0, 4.0, num=8):
    # tee grid search vertical kertoimelle
      z_delay, z_phase = 1.3, 2.3777777777777778
      #TODO: määritä käsin, että jos madon nivelen kulma on ylittänyt nollan niin z painaa alas aina ainakin hetken. ehkä jopa sin wave huipulle asti.
      #TODO: aja uudelleen nykyinen 1.8 alkaen ja num 6 -> 4

      step(prog, dist, z_delay, z_phase)

  """
  for prog in np.array(list(range(1,12)))/15:
    for dist in np.array(list(range(5,13))):
      step(max_prog, max_dist, max_rew, min_prog, min_dist, min_rew)
  """

def save_model(model, optimizer, R_avg, version):
  print(f"Saving checkpoint", version)

  R_avg_str = str(np.round(R_avg,2))
  
  time_str = strftime("%Y%m%d%H%M%S", gmtime())
  torch.save({
      'model': model.state_dict(),
      'optimizer': optimizer.state_dict()
      }, f"model_params/{version}_{time_str}_{R_avg_str}")


def load_model(file_name, model, optimizer, lr = None, load_old_params = False):
  def update_lr(lr, optimizer):
    for param_group in optimizer.param_groups:
      param_group['lr'] = lr

    return optimizer

  if load_old_params:
    checkpoint = torch.load("model_params/"+file_name)
    model.load_state_dict(checkpoint['model'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    
    print("loaded params")
    if lr is not None:
      return update_lr(lr, optimizer)

  return optimizer, model