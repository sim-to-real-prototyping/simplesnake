from collections import namedtuple

class StateInfo:
  def __init__(self):
    self.joints = {}
    elastic_joints = namedtuple("elastic_joints",('horizontal', 'vertical'))([],[])

    self.elastic_joints = elastic_joints
    self.movent_quantity_hor = 1.
    self.movent_quantity_ver = 0.2
    self.force = 10
    self.max_force_index = 2
    self.bounce_force = 100


  def add_elastic_joint_ver(self, joint):
    self.elastic_joints.vertical.append(joint)
    self.joints[joint] = {"position": 0, "force": 0}


  def add_elastic_joint_hor(self, joint):
    self.elastic_joints.horizontal.append(joint)
    self.joints[joint] = {"position": 0, "force": 0}

  def set_joint_status(self, joint, position, force=None):
    self.joints[joint]["position"] = position
    if force is not None:
      self.joints[joint]["force"] = force

  def action_to_position_and_force(self, action, movent_quantity):
    """
    if action == 0:
      pos = movent_quantity
    elif action == 1:
      pos = movent_quantity / 1.5
    elif action == 2:
      pos = 0
    elif action == 3:
      pos = -movent_quantity / 1.5
    elif action == 4:
      pos = -movent_quantity
    else:
      raise Exception("illegal action")
    """

    pos = action*movent_quantity

    return pos, self.force

  # TODO: tämän voi ehkä korvata collision konfauksilla TAI p.calculateInverseDynamics
  def collision_force(self, joint, position, velocity, movent_quantity):
    def get_additional_force(position):
      return ((abs(position) - movent_quantity)*100)**2 * self.bounce_force
    target_force = self.joints[joint]["force"]

    if position > -movent_quantity and position < movent_quantity:
      return target_force
    else:
      additional_force = get_additional_force(position)

      if position < -movent_quantity:
        if velocity < 0:
          return target_force + additional_force
        else: 
          return target_force + additional_force/10
      elif position > movent_quantity:
        if velocity > 0:
          return target_force + additional_force
        else:
          return target_force + additional_force/10
          
      else:
        raise Exception(f"What just happenned? {joint}, {position}, {velocity}")

  def unckecked_bounce_upper(self, joint, position):
    # force > 31
    # position < 2.2
    # target > 0
    return not self.is_free_move(joint) and self.is_free_move_pos(position) and self.is_higher(position)
  
  def unckecked_bounce_lower(self, joint, position):
    # force > 31
    # position > -2.2
    # target < 0
    return not self.is_free_move(joint) and self.is_free_move_pos(position) and self.is_lower(position)

  def unckecked_wall_upper(self, joint, position):
    # force < 31
    # position > 2.2
    # target > 0
    return self.is_free_move(joint) and not self.is_free_move_pos(position) and self.is_higher(position)

  def unckecked_wall_lower(self, joint, position):
    # force < 31
    # position < -2.2
    # target < 0
    return self.is_free_move(joint) and not self.is_free_move_pos(position) and self.is_lower(position)

  def is_lower(self, position):
    return position < -0.1

  def is_higher(self, position):
    return position > 0.1

  def is_free_move_pos(self, position):
    error_marginal = 0.1
    return position < self.movent_quantity + error_marginal and position > -self.movent_quantity - error_marginal

  def is_free_move(self, joint):
    return self.joints[joint]["force"] < self.force + 1
